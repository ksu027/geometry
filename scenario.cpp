
#include <iostream>

#include "scenario.h"
#include "testtorus.h"


// hidmanager
#include "hidmanager/defaulthidmanager.h"

// gmlib
#include <scene/light/gmpointlight.h>
#include <scene/sceneobjects/gmpathtrack.h>
#include <scene/sceneobjects/gmpathtrackarrows.h>

// qt
#include <QQuickItem>

//curves from GMlib
#include <parametrics/curves/gmpbutterfly.h>
#include <parametrics/curves/gmpbsplinecurve.h>

//my model curves
#include "curves/gmplissajous.h"
#include "curves/gmptrefoil.h"
#include "curves/gmpbasin.h"
#include "curves/gmpclelia.h"

//my parametric curves
#include "curves/bspline.h"
#include "curves/blendcurve.h"
#include "curves/gerbscurve.h"

//surfaces from GMlib for the testing
#include "parametrics/surfaces/gmpapple.h"
#include "parametrics/surfaces/gmpbeziercurvesurf.h"
#include "parametrics/surfaces/gmpcone.h"
#include <parametrics/surfaces/gmptrianguloidtrefoil.h>
#include <parametrics/surfaces/gmpcylinder.h>
#include <parametrics/surfaces/gmpsphere.h>
#include <parametrics/surfaces/gmptorus.h>

//my parametric surface
#include "surfaces/gerbssurf.h"

//givven from the course
#include "surfaces/simplesubsurf.h"

#include <QDebug>

//visualizers
#include "parametrics/visualizers/gmpsurfnormalsvisualizer.h"

template <typename T>
inline
std::ostream& operator<<(std::ostream& out, const std::vector<T>& v) {
  out << v.size() << std::endl;
  for(uint i=0; i<v.size(); i++) out << " " << v[i];
  out << std::endl;
  return out;
}




void Scenario::initializeScenario() {

  // Insert a light
  GMlib::Point<GLfloat,3> init_light_pos( 2.0, 4.0, 10 );
  GMlib::PointLight *light = new GMlib::PointLight(  GMlib::GMcolor::white(), GMlib::GMcolor::white(),
                                                     GMlib::GMcolor::white(), init_light_pos );
  light->setAttenuation(0.8f, 0.002f, 0.0008f);
  this->scene()->insertLight( light, false );

  // Insert Sun
  this->scene()->insertSun();

  // Default camera parameters
  int init_viewport_size = 600;
  GMlib::Point<float,3>  init_cam_pos( 0.0f, 0.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_dir( 0.0f, 1.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_up(  1.0f, 0.0f, 0.0f );

  // Projection cam
  auto proj_rcpair = createRCPair("Projection");
  proj_rcpair.camera->set(init_cam_pos,init_cam_dir,init_cam_up);
  proj_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  proj_rcpair.camera->rotateGlobal( GMlib::Angle(-45), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ) );
  proj_rcpair.camera->translateGlobal( GMlib::Vector<float,3>( 0.0f, -20.0f, 20.0f ) );
  scene()->insertCamera( proj_rcpair.camera.get() );
  proj_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );


  /***************************************************************************
   *                                                                         *
   * Standar example, including path track and path track arrows             *
   *                                                                         *
   ***************************************************************************/

  GMlib::Material mm(GMlib::GMmaterial::polishedBronze());
  mm.set(45.0);

  //model curves
  auto lissajous = new PLissajous<float>();
  lissajous->toggleDefaultVisualizer();
  lissajous->sample(2000,1);
  lissajous->setColor(GMlib::Color(255,255,0));

  auto trefoil = new PTrefoil<float>();
  trefoil->toggleDefaultVisualizer();
  trefoil->sample(200,1);
  trefoil->setColor(GMlib::Color(255,255,0));

  auto basin = new PBasin<float>(50);
  basin->toggleDefaultVisualizer();
  basin->sample(2000,1);
  basin->setColor(GMlib::Color(255,255,0));
  basin->translate(GMlib::Vector<double,3> (0.0,0.0,-30.0));

  auto clelia = new PClelia<float>();
  clelia->toggleDefaultVisualizer();
  clelia->sample(2000,1);
  clelia->setColor(GMlib::Color(255,255,0));

  GMlib::DVector<GMlib::Vector<float, 3>> cp(6);
  cp[0] = GMlib::Vector<float, 3>(-10, 2, 10);
  cp[1] = GMlib::Vector<float, 3>(-8, 0, 8);
  cp[2] = GMlib::Vector<float, 3>(-4, -5, 4);
  cp[3] = GMlib::Vector<float, 3>(-1, 4, 1);
  cp[4] = GMlib::Vector<float, 3>(1, 2, -1);
  cp[5] = GMlib::Vector<float, 3>(0, 0, 0);
  int sample_size = 200;
  auto bspline_org = new GMlib::PBSplineCurve<float>(cp,2,false);
  bspline_org->toggleDefaultVisualizer();
  bspline_org->showSelectors();
  bspline_org->sample(sample_size,1);
  bspline_org->setColor(GMlib::Color(255,0,255));
  this->scene()->insert(bspline_org);


  GMlib::DVector<GMlib::Vector<float, 3>> sp;
  for (auto i=0;i<sample_size;i++)
  {
      sp.push_back((*bspline_org).getPosition(bspline_org->getParStart()+i*bspline_org->getParDelta()/sample_size));
  }

  auto bspline_custom_cp = new NewCurves::BSpline<float>(cp);
  bspline_custom_cp->toggleDefaultVisualizer();
  bspline_custom_cp->showSelectors(0.5);
  bspline_custom_cp->sample(20,1);
  bspline_custom_cp->setColor(GMlib::Color(255,255,0));
  this->scene()->insert(bspline_custom_cp);

  auto bspline_custom_sp = new NewCurves::BSpline<float>(sp,6);
  bspline_custom_sp->toggleDefaultVisualizer();
  bspline_custom_sp->sample(20,1);
  bspline_custom_sp->setColor(GMlib::Color(0,255,255));
  //bspline_custom_sp->translate(GMlib::Vector<double,3> (0.0,0.0,0.2));
  bspline_custom_sp->showSelectors(0.5);

  cp.push_back(GMlib::Vector<float, 3>(-4, 0, -1));
  cp.push_back({4, -3, 0});
  cp.push_back({6, 0, 7});
  cp.push_back({5, 5, 1});
  cp.push_back({1, 9, -1});

  auto bspline_org_2 = new GMlib::PBSplineCurve<float>(cp,2,false);
  bspline_org_2->toggleDefaultVisualizer();
  bspline_org_2->setColor(GMlib::Color(0,0,255));
  bspline_org_2->sample(sample_size,1);
  bspline_org_2->showSelectors();
  bspline_org_2->translate(GMlib::Vector<double,3> (5.0,0.0,4.0));

  auto blend_curve = new NewCurves::BlendCurve<float> (bspline_org,bspline_org_2,0.2);
  blend_curve->toggleDefaultVisualizer();
  blend_curve->sample(sample_size*2,1);
  blend_curve->setColor(GMlib::Color(255,0,0));
  blend_curve->setLineWidth(10);

  auto gerbs_curve_tr = new NewCurves::GERBSCurve<float> (trefoil,20);
  auto gerbs_curve_lis = new NewCurves::GERBSCurve<float> (lissajous,20);
  //this is nice
  auto gerbs_curve_ba = new NewCurves::GERBSCurve<float> (basin,20);
  auto gerbs_curve = new NewCurves::GERBSCurve<float> (clelia,20);

  gerbs_curve->toggleDefaultVisualizer();
  gerbs_curve->sample(sample_size*10,1);
  gerbs_curve->setColor(GMlib::Color(0,0,255));
  gerbs_curve->affineTransform(true);

  gerbs_curve_tr->toggleDefaultVisualizer();
  gerbs_curve_tr->sample(sample_size*10,1);
  gerbs_curve_tr->setColor(GMlib::Color(0,0,255));

  gerbs_curve_lis->toggleDefaultVisualizer();
  gerbs_curve_lis->sample(sample_size*10,1);
  gerbs_curve_lis->setColor(GMlib::Color(0,0,255));

  gerbs_curve_ba->toggleDefaultVisualizer();
  gerbs_curve_ba->sample(sample_size*10,1);
  gerbs_curve_ba->setColor(GMlib::Color(0,0,255));



  //==============================================
  //===================SURFACES===================
  //==============================================
  //to build a surface based on given curves
  GMlib::Array<GMlib::PCurve<float,3>*> cu;
  cu.push_back(bspline_org);
  cu.push_back(bspline_org_2);
  auto test_b_surf = new GMlib::PBezierCurveSurf<float> (cu);
  test_b_surf->toggleDefaultVisualizer();
  test_b_surf->sample(25,25,1,1);
  test_b_surf->setMaterial(GMlib::GMmaterial::ruby());

  //to check that U is closed
  auto test_b_surf_u = new GMlib::PSphere<float>();
  test_b_surf_u->toggleDefaultVisualizer();
  test_b_surf_u->sample(25,25,1,1);
  test_b_surf_u->setMaterial(GMlib::GMmaterial::ruby());

  //to check that V is closed
  auto test_b_surf_v = new GMlib::PCylinder<float>();
  test_b_surf_v->toggleDefaultVisualizer();
  test_b_surf_v->sample(25,25,1,1);
  test_b_surf_v->setMaterial(GMlib::GMmaterial::ruby());

  //to check that both U and V are closed
  //auto test_b_surf = new GMlib::PTrianguloidTrefoil<float>();
  auto test_b_surf_uv = new GMlib::PTorus<float> ();
  test_b_surf_uv->toggleDefaultVisualizer();
  test_b_surf_uv->sample(25,25,1,1);
  test_b_surf_uv->setMaterial(GMlib::GMmaterial::ruby());

  auto gerbs_surf = new NewSurf::GERBSSurf<float> (test_b_surf,4,3);
  gerbs_surf->toggleDefaultVisualizer();
  gerbs_surf->sample(25,25,1,1);
  gerbs_surf->setMaterial(GMlib::GMmaterial::emerald());
  auto norms = new GMlib::PSurfNormalsVisualizer<float,3> ();
  gerbs_surf->insertVisualizer(norms);

  auto gerbs_surf_u = new NewSurf::GERBSSurf<float> (test_b_surf_u,4,3);
  gerbs_surf_u->toggleDefaultVisualizer();
  gerbs_surf_u->sample(25,25,1,1);
  gerbs_surf_u->setMaterial(GMlib::GMmaterial::emerald());
  gerbs_surf_u->insertVisualizer(norms);

  auto gerbs_surf_v = new NewSurf::GERBSSurf<float> (test_b_surf_v,4,3);
  gerbs_surf_v->toggleDefaultVisualizer();
  gerbs_surf_v->sample(25,25,1,1);
  gerbs_surf_v->setMaterial(GMlib::GMmaterial::emerald());
  gerbs_surf_v->insertVisualizer(norms);

  auto gerbs_surf_uv = new NewSurf::GERBSSurf<float> (test_b_surf_uv,4,3);
  gerbs_surf_uv->toggleDefaultVisualizer();
  gerbs_surf_uv->sample(25,25,1,1);
  gerbs_surf_uv->setMaterial(GMlib::GMmaterial::emerald());
  gerbs_surf_uv->insertVisualizer(norms);


  //make a vector of all objects
  //predefined
  _s_objs.push_back(lissajous);             //0
  _s_objs.push_back(trefoil);               //1
  _s_objs.push_back(basin);                 //2
  _s_objs.push_back(clelia);                //3
  _s_objs.push_back(bspline_org);           //4
  _s_objs.push_back(bspline_org_2);         //5

  //implemented
  _s_objs.push_back(bspline_custom_cp);     //6
  _s_objs.push_back(bspline_custom_sp);     //7
  _s_objs.push_back(blend_curve);           //8
  _s_objs.push_back(gerbs_curve_lis);       //9
  _s_objs.push_back(gerbs_curve_tr);        //10
  _s_objs.push_back(gerbs_curve_ba);        //11
  _s_objs.push_back(gerbs_curve);           //12

  //predefined surfaces
  _s_objs.push_back(test_b_surf);           //13
  _s_objs.push_back(test_b_surf_u);         //14
  _s_objs.push_back(test_b_surf_v);         //15
  _s_objs.push_back(test_b_surf_uv);        //16

  //implemented surfaces
  _s_objs.push_back(gerbs_surf);            //17
  _s_objs.push_back(gerbs_surf_u);          //18
  _s_objs.push_back(gerbs_surf_v);          //19
  _s_objs.push_back(gerbs_surf_uv);         //20

}




void Scenario::cleanupScenario() {

}




void Scenario::callDefferedGL() {

  GMlib::Array< const GMlib::SceneObject*> e_obj;
  this->scene()->getEditedObjects(e_obj);

  for(int i=0; i < e_obj.getSize(); i++)
    if(e_obj(i)->isVisible()) e_obj[i]->replot();
}

void Scenario::loadGObject(int ind)
{
    this->scene()->clear();
    switch (ind) {
    //B-spline CP
    case 0:
    {
        this->scene()->insert(_s_objs[4]);
        this->scene()->insert(_s_objs[6]);
        break;
    }
    //B-spline SP
    case 1:
    {
        this->scene()->insert(_s_objs[4]);
        this->scene()->insert(_s_objs[7]);
        break;
    }
    //Blending curve
    case 2:
    {
        this->scene()->insert(_s_objs[4]);
        this->scene()->insert(_s_objs[5]);
        this->scene()->insert(_s_objs[8]);
        break;
    }
    //Trefoil
    case 3:
    {
        this->scene()->insert(_s_objs[1]);
        this->scene()->insert(_s_objs[10]);
        break;
    }
    //Lissajous
    case 4:
    {
        this->scene()->insert(_s_objs[0]);
        this->scene()->insert(_s_objs[9]);
        break;
    }
    //Basin
    case 5:
    {
        this->scene()->insert(_s_objs[2]);
        this->scene()->insert(_s_objs[11]);
        break;
    }
    //Basin
    case 6:
    {
        this->scene()->insert(_s_objs[3]);
        this->scene()->insert(_s_objs[12]);
        break;
    }
    //Custom
    case 7:
    {
        this->scene()->insert(_s_objs[13]);
        this->scene()->insert(_s_objs[17]);
        break;
    }
    //Sphere
    case 8:
    {
        this->scene()->insert(_s_objs[14]);
        this->scene()->insert(_s_objs[18]);
        break;
    }
    //Cylinder
    case 9:
    {
        this->scene()->insert(_s_objs[15]);
        this->scene()->insert(_s_objs[19]);
        break;
    }
    //Torus
    case 10:
    {
        this->scene()->insert(_s_objs[16]);
        this->scene()->insert(_s_objs[20]);
        break;
    }
    }
}

