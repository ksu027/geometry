import QtQuick 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4

import "qrc:/qml/components"

import SceneGraphRendering 1.0

Item {
  id: root

  signal toggleHidBindView
  signal loadGObject(int obj_num)

  onToggleHidBindView: hid_bind_view.toggle()

  Renderer {
    id: renderer

    anchors.fill: parent

    rcpair_name: rc_pair_cb.currentText

    ComboBox {
      id: rc_pair_cb
      anchors.top: parent.top
      anchors.left: parent.left
      anchors.margins: 5

      width: 128

      opacity: 0.7

      model: rc_name_model
      textRole: "display"
    }

    Button {
      text: "?"
      anchors.top: parent.top
      anchors.right: parent.right
      anchors.margins: 5

      width: height
      opacity: 0.7

      onClicked: hid_bind_view.toggle()
      id: but_question
    }



    ComboBox {
        id: combo_curve
        currentIndex: 0
        anchors.top: parent.top
        anchors.right: but_question.left
        anchors.margins: 5

        width: 200
        opacity: 0.7
        model: ListModel {
            id: cbItems
            ListElement { text: "B-spline curve control points";}
            ListElement { text: "B-spline curve segments";}
            ListElement { text: "Blending curve";}
            ListElement { text: "GERBS curve Trefoil";}
            ListElement { text: "GERBS curve Lissajous";}
            ListElement { text: "GERBS curve Basin";}
            ListElement { text: "GERBS curve Clelia";}
            ListElement { text: "GERBS surface Custom";}
            ListElement { text: "GERBS surface Sphere";}
            ListElement { text: "GERBS surface Cylinder";}
            ListElement { text: "GERBS surface Torus"; }
        }
        //onCurrentIndexChanged: console.debug('  hello '+(currentIndex))
        onCurrentIndexChanged: loadGObject(currentIndex)
    }


    HidBindingView {
      id: hid_bind_view
      anchors.fill: parent
      anchors.margins: 50
      visible:false

      states: [
        State{
          name: "show"
          PropertyChanges {
            target: hid_bind_view
            visible: true
          }
        }
      ]

      function toggle() {

        if(state === "") state = "show"
        else state = ""

      }
    }

  }

}

