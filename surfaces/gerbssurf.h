#ifndef NEWSURF_GERBSSURF_H
#define NEWSURF_GERBSSURF_H

#include <parametrics/gmpcurve.h>
#include <parametrics/curves/gmpsubcurve.h>

#include <parametrics/gmpsurf.h>
#include <parametrics/surfaces/gmpbsplinesurf.h>
#include <parametrics/surfaces/gmpsubsurf.h>

#include "simplesubsurf.h"

#include <QtDebug>
#include <memory>

#include <random>

namespace NewSurf {

template <typename T>
class GERBSSurf : public GMlib::PSurf<T,3> {
    GM_SCENEOBJECT(GERBSSurf)

    public:
        // gets a model Surface and number of curves in one direction and in another direction (u,v)
        GERBSSurf(GMlib::PSurf<T,3> * surf_org, const int n, const int m);

        bool isClosedU()  const override;
        bool isClosedV()  const override;
    protected:
        void eval(T u, T v, int d1, int d2, bool lu = true, bool lv = true) const;
        void localSimulate(double dt) override;
        T getStartPU() const override;
        T getStartPV() const override;
        T getEndPU() const override;
        T getEndPV() const override;
    private:
        T calcW(const int& d, const int& i, T t, const std::vector<T> * knots) const;
        T bFun(T t) const;
        T bDiv(T t) const;
        int findI(T t, const std::vector<T> * knots) const;
        void makeSurfs();
        void genKnotVectors();

        GMlib::DMatrix<GMlib::PSurf<T, 3>*> _c; //matrix of local surfaces
        GMlib::PSurf<T, 3>* _org_surf;

        int _n, _m;

        std::vector<T> _tU, _tV;                     // knot points u and v
        const int _d = 1;                           //dimention
        const int _k = 2;                           // order predefined to 2 _d+1


        int _sample_size = 50;

        //for local simulation
        int _frame_count=0;
        double all_t = 0.0;



      };



template <typename T>
GERBSSurf<T>::GERBSSurf(GMlib::PSurf<T, 3>* surf_org, int n, int m)
    : _org_surf(surf_org)
    , _n(n)
    , _m(m)
{

  if (_org_surf->isClosedU()) {
      _m ++;
     }
  if (_org_surf->isClosedV()) {
      _n ++;
     }
  // Set size for the matrix of local surfaces

//  this->setDerivationMethod(GMlib::GM_DERIVATION_METHOD::GM_DERIVATION_DD);

  _c.setDim(_m, _n);

  qDebug()<<"dim1"<<_c.getDim1();
  qDebug()<<"dim2"<<_c.getDim2();

  _tU.resize(_m+_k);
  _tV.resize(_n+_k);

  // Create knot vectors
  genKnotVectors();     // create knot vector for U and V
  makeSurfs();

}

template <typename T>
bool GERBSSurf<T>::isClosedU() const {
  return _org_surf->isClosedU();
}


template <typename T>
bool GERBSSurf<T>::isClosedV() const {
  return _org_surf->isClosedV();
}


template <typename T>
void GERBSSurf<T>::genKnotVectors()
{
    //run it twice, once for U, once for V
    T knot = T(this->getStartPU());
    T knot_step = T(_org_surf->getParDeltaU()/T(_m-1));
    bool isClosed = _org_surf->isClosedU();
    std::vector<T> * t = &_tU;

    for (auto l = 0; l<2; l++)
    {
        if (l==1) {
            //if it was not U, then we calculate for V
            knot = this->getStartPV();
            knot_step = T(_org_surf->getParDeltaV()/T(_n-1));
            isClosed = _org_surf->isClosedV();
            t = & _tV;
        }

        // first two elements - begin of the input curve
        // last two elements - end of the input curve

        t->at(0) = knot;
        t->at(1) = knot;


        knot += T(knot_step);

        // Knots between first two and last two distributed
        for (std::size_t i = 2; i < t->size()-2; i++) {
            t->at(i) = knot;
            knot += T(knot_step);
        }

        // last two elements are the same
        t->at(t->size()-2) = knot;
        t->at(t->size()-1) = knot;

        // if the curve is closed - then change first and last
        if (isClosed) {
            t->at(0) = t->at(1)-(t->at(t->size()-2)-t->at(t->size()-3));
            t->at(t->size()-1) = t->at(t->size()-2)+(t->at(2)-t->at(1));
        }
    }
}

template <typename T>
void GERBSSurf<T>::makeSurfs()
{
    for (auto j=1;j<=_m;j++)
    {
        for (auto i=1;i<=_n;i++)
        {
            _c[j-1][i-1] = new PSimpleSubSurf<T>(_org_surf, _tU[j-1],_tU[j+1],_tU[j],
                                                            _tV[i-1],_tV[i+1],_tV[i]);

            //Check for both u and v: if closed - first one = last one as in gurbs curve
            //rows
//            if (_org_surf->isClosedU() && (j)==(_m))
//            {
//                //qDebug()<<i;
//                _c[_m-1][i-1] = _c[0][i-1];
//                //qDebug()<<_c[0][40];
//            }

//            //columns
//            if (_org_surf->isClosedV() && (i)==(_n))
//                    _c[j][_n-1] = _c[j][0];
            //the last corner with the first corner. do not have to do that, as it will point where it should point anyway
//            if (_org_surf->isClosedV() && _org_surf->isClosedU() && (i-1)==(_n-1) && (i-1)==(_n-1))
//                    _c[_m-1][_n-1] = _c[0][0];
            _c[j-1][i-1]->toggleDefaultVisualizer();
            _c[j-1][i-1]->setCollapsed(true);
            _c[j-1][i-1]->sample(10, 10, 1, 1);
            this->insert(_c[j-1][i-1]);
        }
    }

    //the commented one looked better, but did not work correctly
    if (_org_surf->isClosedU())
    {
        for (auto i=0;i<_n;i++)
        {
            _c[_m-1][i] = _c[0][i];
        }
    }

    if (_org_surf->isClosedV())
    {
        for (auto j=0;j<_m;j++)
        {
            _c[j][_n-1] = _c[j][0];
        }
    }

    //just to be sure))
    if (_org_surf->isClosedV() && _org_surf->isClosedU())
    {
        _c[_m-1][_n-1] = _c[0][0];
    }

    qDebug()<<"m="<<_m<<" n="<<_n;


}

template <typename T>
T GERBSSurf<T>::bFun(T t) const
{
    // Symmetric RB-function
    // the same as in the blending curve
    return pow(t,2)/(2*pow(t,2)-2*t+1);
    //return -2*pow(t,3)+3*pow(t,2);
}

template <typename T>
T GERBSSurf<T>::bDiv(T t) const
{
    // Dirivative of the bFun
    return (2*t)/(2*pow(t,2)-2*t+1);
    //return -6*pow(t,2)+6*t;
}

template <typename T>
void GERBSSurf<T>::eval( T u, T v, int d1, int d2, bool lu, bool lv ) const

{
    this->_p.setDim(_d+1, _d+1);

    int i_u = findI(u, &_tU);
    int i_v = findI(v, &_tV);


    T b_u = bFun(calcW(1, i_u, u, &_tU));
    T b_v = bFun(calcW(1, i_v, v, &_tV));

    //derivatives
    T bd_u = bDiv(calcW(1, i_u, u, &_tU));
    T bd_v = bDiv(calcW(1, i_v, v, &_tV));

    //
    // (1-b_u  b_u)*(p00 p01)*(1-b_v)
    //              (p10 p11) ( b_v )
    //
    auto p00 = _c(i_u-1)(i_v-1)->evaluateParent(u, v, d1, d2);
    auto p10 = _c(i_u)(i_v-1)->evaluateParent(u, v, d1, d2);
    auto p01 = _c(i_u-1)(i_v)->evaluateParent(u, v, d1, d2);
    auto p11 = _c(i_u)(i_v)->evaluateParent(u, v, d1, d2);


    //inner sum
    auto A = ((1 - b_u) * p00[0][0]) + (b_u * p10[0][0]);
    auto B = ((1 - b_u) * p01[0][0]) + (b_u * p11[0][0]);

    //inner sum with u derivatives of b
    auto Ad = ((- bd_u) * p00[0][0]) + (bd_u * p10[0][0]);
    auto Bd = ((- bd_u) * p01[0][0]) + (bd_u * p11[0][0]);

    //inner sum with u derivatives of p
    auto Apdu = ((1 - b_u) * p00[1][0]) + (b_u * p10[1][0]);
    auto Bpdu = ((1 - b_u) * p00[1][0]) + (b_u * p10[1][0]);

    //inner sum with v derivatives of p
    auto Apdv = ((1 - b_u) * p00[0][1]) + (b_u * p10[0][1]);
    auto Bpdv = ((1 - b_u) * p00[0][1]) + (b_u * p10[0][1]);

    this->_p[0][0] = (A*(1 - b_v) + B*b_v);
    this->_p[1][0] = (Ad*(1 - b_v) + Bd*b_v) + Apdu*(1 - b_v) + Bpdu*b_v;
    this->_p[0][1] = (A*(- bd_v) + B*bd_v) + Apdv*(1 - b_v) + Bpdv*b_v;

}


template <typename T>
int GERBSSurf<T>::findI(T t, const std::vector<T> * knots) const
{
    auto i = 1;
    for (;i<knots->size();i++)
    {
        if ((t<knots->at(i) && t>=knots->at(i-1) ) || knots->size()-3< i)
        {
            i-=1;
            break;
        }
    }

    return i;
}

template <typename T>
T GERBSSurf<T>::calcW(const int& d, const int& i, T t, const std::vector<T> * knots) const
{
    // W_{d,i}(t) = (t - t_i)/(t_{i+d} - t_i)
    return (t - knots->at(i)) / (knots->at(i + d) - knots->at(i));
}

template <typename T>
T GERBSSurf<T>::getStartPU() const
{
  return _org_surf->getParStartU();
}

template <typename T>
T GERBSSurf<T>::getStartPV() const
{
  return _org_surf->getParStartV();
}

template <typename T>
T GERBSSurf<T>::getEndPU() const
{
  return _org_surf->getParEndU();
}

template <typename T>
T GERBSSurf<T>::getEndPV() const
{
  return _org_surf->getParEndV();
}

template <typename T>
void GERBSSurf<T>::localSimulate( double dt ) {
//    std::random_device rd; // obtain a random number from hardware
//    std::mt19937 eng(rd()); // seed the generator
//    std::uniform_int_distribution<> distrn(0, _n-1); // define the range
//    std::uniform_int_distribution<> distrm(0, _m-1); // define the range

//    std::vector<int> ms,ns;
//    int m_n = distrm(eng);
//    int n_n = distrn(eng);

//    for (auto j =0; j<m_n;j++)
//    {
//        ms.push_back(distrm(eng));
//    }
//    for (auto i =0; i<n_n;i++)
//    {
//        ns.push_back(distrn(eng));
//    }

//    for (auto j=0;j<ms.size();j++)
//        for (auto i=0;i<ns.size();i++)
//            _c[ms[j]][ns[i]]->translate(GMlib::Vector<float,3>(0.0,0.0,sin(_frame_count)));



//    //simplified version - update every some frames
//    _frame_count++;
//    all_t+=dt;
//    if (_frame_count<_n-1){


//        //int j=9*abs(sin(_frame_count));
//        //int i=9*abs(cos(_frame_count));
//        int j=_frame_count;

//        for (auto j=0;j<_n;j++)
//            for (auto i=0;i<_m;i++)
//            {
//                //_c[i][j]->translate(GMlib::Vector<float,3>(sin(all_t),cos(all_t),0.0));
//                _c[i][j]->rotate(GMlib::Angle(dt),GMlib::Vector<float,3>(1.0,0.0,1.0));
//            }

        this->sample(this->_visu(0)(0)(0),this->_visu(0)(0)(1),1,1);
        //this->setEditDone();
//        //_frame_count = 0;
//    }
//    else
//        _frame_count=0;
//
}

};







//#include "GERBSSurf.c"

#endif // GERBSSurf_H
