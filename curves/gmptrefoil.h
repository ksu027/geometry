#ifndef GM_PARAMETRICS_CURVES_PTREFOIL_H
#define GM_PARAMETRICS_CURVES_PTREFOIL_H

#include <parametrics/gmpcurve.h>

//namespace GMlib {

  template <typename T>
  class PTrefoil : public GMlib::PCurve<T,3> {
    GM_SCENEOBJECT(PTrefoil)
  public:
    PTrefoil( T size = T(5) );
    PTrefoil( const PTrefoil<T>& copy );


    //****************************************
    //****** Virtual public functions   ******
    //****************************************

    // from PCurve
    bool          isClosed() const override;

  protected:
    // Virtual functions from PCurve, which have to be implemented locally
    void          eval(T t, int d, bool l) const override;
    T             getStartP() const override;
    T             getEndP()   const override;


    // Protected data for the curve
    T             _size;


//    void          localSimulate(double dt) override;

  }; // END class PTrefoil

//} // END namepace GMlib



  template <typename T>
  PTrefoil<T>:: PTrefoil (T size ) :GMlib::PCurve<T,3 >(20 ,0 ,0) ,_size ( size ) {
  // Note that the last parameter in the PCurve constructor is 2,
  // this because 2 derivatives in eval () is implemented !!!!
  }

  // ∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗
  // Copy constructor
  // ∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗
  template <typename T>
  PTrefoil<T>:: PTrefoil ( const PTrefoil<T>& copy) :GMlib::PCurve<T,3>(copy ),_size (copy._size){}


//***************************************************
// Overrided (public) virtual functons from PCurve **
//***************************************************


  /*! bool PTrefoil<T>::isClosed() const
  *  To tell that this curve (Trefoil) is closed.
  */
  template <typename T>
  bool PTrefoil<T>::isClosed() const {
      return true;
  }



//******************************************************
// Overrided (protected) virtual functons from PCurve **
//******************************************************

/*! void PTrefoil<T>::eval( T t, int d, bool l ) const
 *  Evaluation of the curve at a given parameter value
 *  To compute position and d derivatives at parameter value t on the curve.
 *  2 derivatives are implemented
 *
 *  \param  t[in]  The parameter value to evaluate at
 *  \param  d[in]  The number of derivatives to compute
 *  \param  l[in]  (dummy) because left and right are always equal
 */
  template <typename T>
  void PTrefoil<T>::eval( T t, int d, bool /*l*/ ) const {

  this->_p.setDim( d + 1 );

      const double epsilon = 1.0; //right handed +1, left handed -1

      const double x   = cos(t)+2*cos(2*t);
      const double y   = sin(t)-2*sin(2*t);
      const double z   = 2*epsilon*sin(3*t);

//      const double x   = sin(2*t);
//      const double y   = sin(t)*cos(2*t);
//      const double z   = epsilon*cos(3*t);



      this->_p[0][0] = _size * T(x);
      this->_p[0][1] = _size * T(y);
      this->_p[0][2] = _size * T(z);
}



/*! T PTrefoil<T>::getStartP() const
 *  Provides the start parameter value associated with
 *  the eval() function implemented above.
 *  (the start parameter value = 0).
 *
 *  \return The parametervalue at start of the internal domain
 */
template <typename T>
  T PTrefoil<T>::getStartP() const {
  return T(0);
}



/*! T PTrefoil<T>::getEndP() const
 *  Provides the end parameter value associated with
 *  the eval() function implemented above.
 *  (the end parameter value = 24*Pi).
 *
 *  \return The parametervalue at end of the internal domain
 */
template <typename T>
  T PTrefoil<T>::getEndP() const {
  return T( M_PI * 2.0 );
}

#endif // GM_PARAMETRICS_CURVES_PTrefoil_H

