#ifndef GM_PARAMETRICS_CURVES_PBASIN_H
#define GM_PARAMETRICS_CURVES_PBASIN_H

#include <parametrics/gmpcurve.h>

//namespace GMlib {

  template <typename T>
  class PBasin : public GMlib::PCurve<T,3> {
    GM_SCENEOBJECT(PBasin)
  public:
    PBasin( T size = T(5) );
    PBasin( const PBasin<T>& copy );


    //****************************************
    //****** Virtual public functions   ******
    //****************************************

    // from PCurve
    bool          isClosed() const override;

  protected:
    // Virtual functions from PCurve, which have to be implemented locally
    void          eval(T t, int d, bool l) const override;
    T             getStartP() const override;
    T             getEndP()   const override;


    // Protected data for the curve
    T             _size;


//    void          localSimulate(double dt) override;

  }; // END class PBasin

//} // END namepace GMlib



  template <typename T>
  PBasin<T>:: PBasin (T size ) :GMlib::PCurve<T,3 >(20 ,0 ,0) ,_size ( size ) {
  // Note that the last parameter in the PCurve constructor is 2,
  // this because 2 derivatives in eval () is implemented !!!!
  }

  // ∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗
  // Copy constructor
  // ∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗
  template <typename T>
  PBasin<T>:: PBasin ( const PBasin<T>& copy) :GMlib::PCurve<T,3>(copy ),_size (copy._size){}


//***************************************************
// Overrided (public) virtual functons from PCurve **
//***************************************************


  /*! bool PBasin<T>::isClosed() const
  *  To tell that this curve (Basin) is closed.
  */
  template <typename T>
  bool PBasin<T>::isClosed() const {
      return true;
  }



//******************************************************
// Overrided (protected) virtual functons from PCurve **
//******************************************************

/*! void PBasin<T>::eval( T t, int d, bool l ) const
 *  Evaluation of the curve at a given parameter value
 *  To compute position and d derivatives at parameter value t on the curve.
 *  2 derivatives are implemented
 *
 *  \param  t[in]  The parameter value to evaluate at
 *  \param  d[in]  The number of derivatives to compute
 *  \param  l[in]  (dummy) because left and right are always equal
 */
  template <typename T>
  void PBasin<T>::eval( T t, int d, bool /*l*/ ) const {

  this->_p.setDim( d + 1 );

      const double n = 2.5;

      const double x   = cos(t)*cos(n*t);
      const double y   = sin(t)*cos(n*t);
      const double z   = pow(cos(n*t),2);

      const double a = 1.0;
      const double b = 1.0;

      this->_p[0][0] = _size * T(x * a);
      this->_p[0][1] = _size * T(y * a);
      this->_p[0][2] = _size * T(z * b);
}



/*! T PBasin<T>::getStartP() const
 *  Provides the start parameter value associated with
 *  the eval() function implemented above.
 *  (the start parameter value = 0).
 *
 *  \return The parametervalue at start of the internal domain
 */
template <typename T>
  T PBasin<T>::getStartP() const {
  return T(0);
}



/*! T PBasin<T>::getEndP() const
 *  Provides the end parameter value associated with
 *  the eval() function implemented above.
 *  (the end parameter value = 24*Pi).
 *
 *  \return The parametervalue at end of the internal domain
 */
template <typename T>
  T PBasin<T>::getEndP() const {
  return T( M_PI * 4.0 );
}

#endif // GM_PARAMETRICS_CURVES_PBasin_H

