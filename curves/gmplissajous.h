#ifndef GM_PARAMETRICS_CURVES_PLISSAJOUS_H
#define GM_PARAMETRICS_CURVES_PLISSAJOUS_H

#include <parametrics/gmpcurve.h>

//namespace GMlib {

  template <typename T>
  class PLissajous : public GMlib::PCurve<T,3> {
    GM_SCENEOBJECT(PLissajous)
  public:
    PLissajous( T size = T(5) );
    PLissajous( const PLissajous<T>& copy );


    //****************************************
    //****** Virtual public functions   ******
    //****************************************

    // from PCurve
    bool          isClosed() const override;

  protected:
    // Virtual functions from PCurve, which have to be implemented locally
    void          eval(T t, int d, bool l) const override;
    T             getStartP() const override;
    T             getEndP()   const override;


    // Protected data for the curve
    T             _size;


//    void          localSimulate(double dt) override;

  }; // END class PLissajous

//} // END namepace GMlib



  template <typename T>
  PLissajous<T>:: PLissajous (T size ) :GMlib::PCurve<T,3 >(20 ,0 ,0) ,_size ( size ) {
  // Note that the last parameter in the PCurve constructor is 2,
  // this because 2 derivatives in eval () is implemented !!!!
  }

  // ∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗
  // Copy constructor
  // ∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗
  template <typename T>
  PLissajous<T>:: PLissajous ( const PLissajous<T>& copy) :GMlib::PCurve<T,3>(copy ),_size (copy._size){}


//***************************************************
// Overrided (public) virtual functons from PCurve **
//***************************************************


  /*! bool PLissajous<T>::isClosed() const
  *  To tell that this curve (Lissajous) is closed.
  */
  template <typename T>
  bool PLissajous<T>::isClosed() const {
      return true;
  }



//******************************************************
// Overrided (protected) virtual functons from PCurve **
//******************************************************

/*! void PLissajous<T>::eval( T t, int d, bool l ) const
 *  Evaluation of the curve at a given parameter value
 *  To compute position and d derivatives at parameter value t on the curve.
 *  2 derivatives are implemented
 *
 *  \param  t[in]  The parameter value to evaluate at
 *  \param  d[in]  The number of derivatives to compute
 *  \param  l[in]  (dummy) because left and right are always equal
 */
  template <typename T>
  void PLissajous<T>::eval( T t, int d, bool /*l*/ ) const {

  this->_p.setDim( d + 1 );

//  const double n = 0.5;
//  const double m = 0.9;
//  const double fi = 0.0;
//  const double tetta = 0.0;

//  const double x   = sin(t);
//  const double y   = sin(n*t+fi);
//  const double z   = sin(m*t+tetta);

//  const double a = 1.0;
//  const double b = 1.0;
//  const double c = 1.0;


//  this->_p[0][0] = _size * T(x * a);
//  this->_p[0][1] = _size * T(y * b);
//  this->_p[0][2] = _size * T(z * c);

//version 2

//      const double nx = 3.0;
//      const double ny = 4.0;
//      const double nz = 7.0;

//      const double phix = 0.0;
//      const double phiy = 0.0;
//      const double phiz = 0.0;

//      const double x   = cos(nx*t+phix);
//      const double y   = cos(ny*t+phiy);
//      const double z   = cos(nz*t+phiz);

//      const double a = 1.0;
//      const double b = 1.0;
//      const double c = 1.0;

//one more

//      const double nx = 3.0;
//      const double ny = 2.0;
//      const double nz = 7.0;

//      const double phix = 0.0;
//      const double phiy = 0.0;
//      const double phiz = 0.0;

//      const double x   = cos(nx*t+phix);
//      const double y   = sin(ny*t+phiy);
//      const double z   = sin(nz*t+phiz);

//      const double a = 1.0;
//      const double b = 1.0;
//      const double c = 1.0;

      const double nx = 3.0;
      const double ny = 2.0;
      const double nz = 7.0;

      const double phix = 0.0;
      const double phiy = 0.0;
      const double phiz = 0.0;

      const double x   = sin(nx*t+phix);
      const double y   = sin(ny*t+phiy);
      const double z   = sin(nz*t+phiz);

      const double a = 1.0;
      const double b = 1.0;
      const double c = 1.0;

      this->_p[0][0] = _size * T(x * a);
      this->_p[0][1] = _size * T(y * b);
      this->_p[0][2] = _size * T(z * c);
}



/*! T PLissajous<T>::getStartP() const
 *  Provides the start parameter value associated with
 *  the eval() function implemented above.
 *  (the start parameter value = 0).
 *
 *  \return The parametervalue at start of the internal domain
 */
template <typename T>
  T PLissajous<T>::getStartP() const {
  return T(0);
}



/*! T PLissajous<T>::getEndP() const
 *  Provides the end parameter value associated with
 *  the eval() function implemented above.
 *  (the end parameter value = 24*Pi).
 *
 *  \return The parametervalue at end of the internal domain
 */
template <typename T>
  T PLissajous<T>::getEndP() const {
  return T( M_PI * 2.0 );
}

#endif // GM_PARAMETRICS_CURVES_PLissajous_H

