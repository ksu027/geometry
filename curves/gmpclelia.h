#ifndef GM_PARAMETRICS_CURVES_PCLELIA_H
#define GM_PARAMETRICS_CURVES_PCLELIA_H

#include <parametrics/gmpcurve.h>

#include <QDebug>

//namespace GMlib {

  template <typename T>
  class PClelia : public GMlib::PCurve<T,3> {
    GM_SCENEOBJECT(PClelia)
  public:
    PClelia( T size = T(5) );
    PClelia( const PClelia<T>& copy );


    //****************************************
    //****** Virtual public functions   ******
    //****************************************

    // from PCurve
    bool          isClosed() const override;

  protected:
    // Virtual functions from PCurve, which have to be implemented locally
    void          eval(T t, int d, bool l) const override;
    T             getStartP() const override;
    T             getEndP()   const override;


    // Protected data for the curve
    T             _size;
    double _all_time=0;

    void          localSimulate(double dt) override;


//    void          localSimulate(double dt) override;

  }; // END class PClelia

//} // END namepace GMlib



  template <typename T>
  PClelia<T>:: PClelia (T size ) :GMlib::PCurve<T,3 >(20 ,0 ,0) ,_size ( size ) {
  // Note that the last parameter in the PCurve constructor is 2,
  // this because 2 derivatives in eval () is implemented !!!!
  }

  // ∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗
  // Copy constructor
  // ∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗
  template <typename T>
  PClelia<T>:: PClelia ( const PClelia<T>& copy) :GMlib::PCurve<T,3>(copy ),_size (copy._size){}


//***************************************************
// Overrided (public) virtual functons from PCurve **
//***************************************************


  /*! bool PClelia<T>::isClosed() const
  *  To tell that this curve (Clelia) is closed.
  */
  template <typename T>
  bool PClelia<T>::isClosed() const {
      return true;
  }



//******************************************************
// Overrided (protected) virtual functons from PCurve **
//******************************************************

/*! void PClelia<T>::eval( T t, int d, bool l ) const
 *  Evaluation of the curve at a given parameter value
 *  To compute position and d derivatives at parameter value t on the curve.
 *  2 derivatives are implemented
 *
 *  \param  t[in]  The parameter value to evaluate at
 *  \param  d[in]  The number of derivatives to compute
 *  \param  l[in]  (dummy) because left and right are always equal
 */
  template <typename T>
  void PClelia<T>::eval( T t, int d, bool /*l*/ ) const {

  this->_p.setDim( d + 1 );

      const double v = 0.0;

      const double p = 20;
      const double q = 19;


      const double x   = cos(q*(t-v))*cos(p*(t+v));
      const double y   = sin(q*(t-v))*cos(p*(t+v));
      const double z   = sin(p*(t+v));

      this->_p[0][0] = _size * T(x);
      this->_p[0][1] = _size * T(y);
      this->_p[0][2] = _size * T(z);
}



/*! T PClelia<T>::getStartP() const
 *  Provides the start parameter value associated with
 *  the eval() function implemented above.
 *  (the start parameter value = 0).
 *
 *  \return The parametervalue at start of the internal domain
 */
template <typename T>
  T PClelia<T>::getStartP() const {
  return T(0);
}



/*! T PClelia<T>::getEndP() const
 *  Provides the end parameter value associated with
 *  the eval() function implemented above.
 *  (the end parameter value = 24*Pi).
 *
 *  \return The parametervalue at end of the internal domain
 */
template <typename T>
  T PClelia<T>::getEndP() const {
  return T( M_PI * 4.0 );
}


template <typename T>
void PClelia<T>::localSimulate( double dt ) {
//    _size=30.0*(1.1+sin(_all_time));
//    _all_time += dt;

    this->resample();
    this->setEditDone();
}

#endif // GM_PARAMETRICS_CURVES_PClelia_H

