#ifndef NEWCURVES_GERBSCURVE_H
#define NEWCURVES_GERBSCURVE_H

#include <parametrics/gmpcurve.h>
#include <parametrics/curves/gmpsubcurve.h>

#include <QtDebug>
#include <memory>

namespace NewCurves {

template <typename T>
class GERBSCurve : public GMlib::PCurve<T,3> {
    GM_SCENEOBJECT(GERBSCurve)

    public:
        // gets PCurve and number of curves
        GERBSCurve(GMlib::PCurve<T,3> * curve_org, const int n_curves);

        bool isClosed() const override;
        void affineTransform(bool in_transform) {_affine_transform=in_transform;}
    protected:
        void eval(T t, int d, bool l) const override;
        T    getStartP() const override;
        T    getEndP() const override;
        void localSimulate(double dt) override;
    private:
        void genKnotVector();
        void makeCurves();
        int  findI(T t) const;
        T    bFun(T t) const;
        T    calcW(const int& d, const int& i, T t) const;
        GMlib::Vector<T, 2> calcBasis(T t, int i) const;

        GMlib::PCurve<T,3> * _curve_org;             // original curve
        std::vector<GMlib::PCurve<T,3>*> _curves;    // vector of local curves
        int _n_curves;                               // number of curves
        const int k = 2;                             // order predefined to 2
        std::vector<T> _t;                           // knot points
        int _sample_size = 50;

        //for local simulation
        int _frame_count=0;
        int _scene_frames=0;
        GMlib::Point<float,3> _rot_center;
        std::vector<GMlib::Vector<float,3>> _c_origins;
        GMlib::Vector<float,3> _to_goal {0.0f,0.0f,0.0f};
        GMlib::Vector<float,3> _to_goal_norm {0.0f,0.0f,0.0f};
        GMlib::Vector<float,3> _cur_pos {0.0f,0.0f,0.0f};
        GMlib::Vector<float,3> _cur_pos2 {0.0f,0.0f,0.0f};

        float _scale_factor = 0.9f;

        //frames limits
        int _f_start=20;
        int _f_wait=20000;
        int _f_next=20000;
        int _scene=0;
        int _next_scene=1;
        std::vector<GMlib::Vector<float,2>> _star {{0.0f,6.23f},{-6.15f,9.46f},{-4.97f,2.61f},{-9.94f,-2.23f},{-3.07f,-3.23f},{0.0f,-9.46f},{3.07f,-3.23f},{9.94f,-2.23f},{4.97f,2.61f},{6.15f,9.46f}};
        //std::map<int,GMlib::PCurve<T,3>&> _curves_to_change;
        std::vector<int> _ctc; //curves to change
        std::vector<float> _scales_ratio;
        std::vector<float> _dist_ratio;
        GMlib::Angle _angle = 1;

        bool _affine_transform = false;

};

template <typename T>
GERBSCurve<T>::GERBSCurve(GMlib::PCurve<T,3> * curve_org, const int n_curves)
    : _curve_org(curve_org)
    , _n_curves(n_curves)
{
    if (_curve_org->isClosed()) {
        _n_curves ++;
    }
    _curves.resize(_n_curves);
    _t.resize(_n_curves+k);
    genKnotVector();
    makeCurves();

    //for visualisation
    _rot_center=_curve_org->getGlobalPos();
    _scales_ratio.resize(_n_curves);
    _dist_ratio.resize(_n_curves);
//    qDebug()<<_rot_center;
}


template <typename T>
bool GERBSCurve<T>::isClosed() const {
    //in our case is always open due to the implemented knot-vector logic
    return _curve_org->isClosed();
}
template <typename T>
void GERBSCurve<T>::genKnotVector()
{
    // first two elements - begin of the input curve
    // last two elements - end of the input curve
    T knot = T(this->getStartP());
    _t[0] = knot;
    _t[1] = knot;

    T knot_step = T((this->getEndP()-this->getStartP())/(_n_curves-1));
    knot += T(knot_step);

    // Knots between first two and last two distributed
    for (std::size_t i = 2; i < _t.size()-2; i++) {
        _t[i] = knot;
        knot += T(knot_step);
    }

    // last two elements are the same
    _t[_t.size()-2] = knot;
    _t[_t.size()-1] = knot;

    // if the curve is closed - then change first and last
    if (_curve_org->isClosed()) {
        _t[0] = _t[1]-(_t[_t.size()-2]-_t[_t.size()-3]);
        _t[_t.size()-1] = _t[_t.size()-2]+(_t[2]-_t[1]);
    }
    // also add that last function is equal to first function
    // it is in makerCurves
}

template <typename T>
void GERBSCurve<T>::makeCurves()
{
    for (int i = 1; i<=_curves.size(); i++)
    {
        //qDebug()<<"s="<<_t[i-1]<<"; e="<<_t[i + 1]<<"; c="<<_t[i];
        _curves[i-1] = new GMlib::PSubCurve<T>(_curve_org, _t[i-1], _t[i + 1], _t[i]);
        if (_curve_org->isClosed() && i==_curves.size())
            _curves[_curves.size()-1]=_curves[0];
        _curves[i-1]->toggleDefaultVisualizer();
        _curves[i-1]->toggleCollapsed();
        _curves[i-1]->sample(100,1);
        this->insert(_curves[i-1]);


    }

    //for visualization
    for (auto i = 0; i<_curves.size(); i++)
    {
        _c_origins.push_back(_curves[i]->getGlobalPos());
    }
    _ctc.resize(_curves.size()-1);
    std::iota (std::begin(_ctc), std::end(_ctc), 0);

}

template <typename T>
int GERBSCurve<T>::findI(T t) const
{

//    //CHECK I selection
    auto i = 1;
    for (;i<_t.size();i++)
    {
        if ((t<_t[i] && t>=_t[i-1] ) /*|| _t.size()-3< i*/)
        {
            i-=1;
            break;
        }
    }

    return i;
}

template <typename T>
T GERBSCurve<T>::calcW(const int& d, const int& i, T t) const
{
    // W_{d,i}(t) = (t - t_i)/(t_{i+d} - t_i)
    return (t - _t[i]) / (_t[i + d] - _t[i]);
}

template <typename T>
T GERBSCurve<T>::getStartP() const
{
    //start of the original curve
    return _curve_org->getParStart();
}

template <typename T>
T GERBSCurve<T>::getEndP() const
{
    //end of original curve
    return _curve_org->getParEnd();
}

template <typename T>
T GERBSCurve<T>::bFun(T t) const
{
    // Symmetric RB-function
    // the same as in the blending curve
    return pow(t,2)/(2*pow(t,2)-2*t+1);
}

template <typename T>
GMlib::Vector<T, 2> GERBSCurve<T>::calcBasis(T t, int i) const
{
    // hardcode calculation of Ws
    auto W_1_i = calcW(1, i, t);

    // hardcode calculation of basis
    GMlib::Vector<T, 2> basis;
    basis[0] = 1 - bFun(W_1_i);
    basis[1] =bFun(W_1_i);

    return basis;
}

template <typename T>
void GERBSCurve<T>::eval(T t, int d, bool /*l*/) const
{
    // in our case d is always 2 => change it hardcoded
    d=2;
    this->_p.setDim(d+1);

    int i = findI(t);
    auto b = calcBasis(t, i);

    //qDebug()<<"t "<<t;
    //qDebug()<<"i "<<i;
    this->_p[0] = b[0] * _curves[i - 1]->evaluateParent(t,0)[0] + b[1] * _curves[i]->evaluateParent(t,0)[0];
}

template <typename T>
void GERBSCurve<T>::localSimulate( double dt ) {

    if (_affine_transform)
    {
    //_curve_org->setMatrix(GMlib::Vector<float,3>(0.0f,sin(0.5*M_PI*dt),cos(0.5*M_PI*dt)));
    float fact=float(M_PI)/10.0f;
    float step=0.0f;
    float go_step = 0.01f;
    GMlib::Vector<float,3> vec_to_c;
    float star_step;
    int all_ready;


    float xd, yd;

    //predefiend scenes
    switch (_scene) {
    /* pause */
    case 0:
       _scene_frames++;
       if (_scene_frames>19)
       {
           _scene = 11;
           _scene_frames=0;
       }
       break;
       //rotate a ball
       case 11:
       {
           _scene_frames++;
          for (auto i=0; i<_ctc.size(); i++)
          {
              _curves[i]->rotateGlobal(GMlib::Angle(5),_rot_center,GMlib::Vector<float,3>(1.0,1.0,1.0));
          }
          if (_scene_frames==72)
          {

              _scene_frames=0;
              _scene = 1;
          }
          break;
       }
    /* move  to center */
        case 1:
        {
            _scene_frames++;
            for (auto i=0; i<_ctc.size(); i++)
            {
                if (_ctc[i]<10)
                {
                    _cur_pos = _curves[_ctc[i]]->getGlobalPos();
                    if (_scene_frames==1)
                    {
                        _dist_ratio[_ctc[i]]=(_curve_org->getGlobalPos()-_curves[_ctc[i]]->getGlobalPos()).getLength();
                    }

                    vec_to_c=_curve_org->getGlobalPos()-_curves[_ctc[i]]->getGlobalPos();
                    if (vec_to_c.getLength()>0.0f)
                        _curves[_ctc[i]]->translateGlobal(vec_to_c*(1.0f/150.0f)*(_dist_ratio[_ctc[i]]/vec_to_c.getLength()));
                        _curves[_ctc[i]+10]->translateGlobal(vec_to_c*(1.0f/150.0f)*(_dist_ratio[_ctc[i]]/vec_to_c.getLength()));
                }
            }
            if (_scene_frames==150)
            {
                _scene_frames=0;
                _scene = 2;
            }
           break;
        }
        /* rotate 360 degrees */
        case 2:
        {
//            qDebug()<<_scene_frames;
           _scene_frames++;
           for (auto i =0;i<_curves.size()-1;i++)
           {
               if (i%2==0)
                   _curves[i]->rotate(GMlib::Angle(2),GMlib::Vector<float,3>(-1.0,1.0,-1.0));
               else
                   _curves[i]->rotate(GMlib::Angle(2),GMlib::Vector<float,3>(1.0,-1.0,1.0));
           }
           if (_scene_frames==180)
           {
               _scene_frames=0;
               _scene=3;
           }
           break;
        }
           /* move to origins */
        case 3:
        {
            _scene_frames++;
            for (auto i=0; i<_ctc.size(); i++)
            {
                if (_ctc[i]<10)
                {
                    _cur_pos = _curves[_ctc[i]]->getGlobalPos();
                    if (_scene_frames==1)
                    {
                        _dist_ratio[_ctc[i]]=(_c_origins[_ctc[i]]-_curves[_ctc[i]]->getGlobalPos()).getLength();
                    }

                    vec_to_c=_c_origins[_ctc[i]]-_curves[_ctc[i]]->getGlobalPos();
                    if (vec_to_c.getLength()>0.0f)
                        _curves[_ctc[i]]->translateGlobal(vec_to_c*(1.0f/150.0f)*(_dist_ratio[_ctc[i]]/vec_to_c.getLength()));
                        _curves[_ctc[i]+10]->translateGlobal(vec_to_c*(1.0f/150.0f)*(_dist_ratio[_ctc[i]]/vec_to_c.getLength()));
                }
            }
            if (_scene_frames==150)
            {
                _scene_frames=0;
                _scene = 4;
            }
           break;
        }
        /* scaling */
        case 4:
        {
            _scene_frames++;
            for (auto i=0; i<_ctc.size(); i++)
            {
               if (_scene_frames==1)
               {
                   _dist_ratio[_ctc[i]]=(_c_origins[_ctc[i]]-_curves[_ctc[i]]->getGlobalPos()).getLength();
                   _scales_ratio[_ctc[i]]=powf(0.0005f/(_curves[_ctc[i]]->getScale()[0]),1/150.0f);
               }

               _curves[_ctc[i]]->scale(_scales_ratio[_ctc[i]]);
               vec_to_c=_c_origins[_ctc[i]]-_curves[_ctc[i]]->getGlobalPos();
               if (vec_to_c.getLength()>0.0f)
                _curves[_ctc[i]]->translateGlobal(vec_to_c*(1.0f/150.0f)*(_dist_ratio[_ctc[i]]/vec_to_c.getLength()));

            }
            if (_scene_frames==150)
            {
               _scene_frames=0;
               _scene = 5;
            }
            break;
        }
        /* make a star */
        case 5:
        {
           _scene_frames++;
           GMlib::Vector<float,3> star;
           for (auto i=0; i<_ctc.size(); i++)
           {
               if (_ctc[i]<10)
               {
                   _cur_pos = _curves[_ctc[i]]->getGlobalPos();
                   star = _cur_pos;
                   star[0] = _star[_ctc[i]][0];
                   star[1] = _star[_ctc[i]][1];

                   if (_scene_frames==1)
                   {
                       _dist_ratio[_ctc[i]]=(star-_curves[_ctc[i]]->getGlobalPos()).getLength();
                   }

                   vec_to_c=star-_cur_pos;
                   qDebug()<<vec_to_c.getLength();
                   if (vec_to_c.getLength()>0.0f)
                   {
                       _curves[_ctc[i]]->translateGlobal(vec_to_c*(1.0f/120.0f)*(_dist_ratio[_ctc[i]]/vec_to_c.getLength()));
                       _curves[_ctc[i]+10]->translateGlobal(vec_to_c*(1.0f/120.0f)*(_dist_ratio[_ctc[i]]/vec_to_c.getLength()));
                   }
               }
           }
           if (_scene_frames==120)
           {
               _scene_frames=0;
               _scene = 6;
           }

           break;
        }
        /* change colors here */
        case 6:
        {

            _scene_frames=0;
            _scene = 7;
           break;
        }

        //star rotate + move to center
        case 7:
        {
            _scene_frames++;
           for (auto i=0; i<_ctc.size(); i++)
           {
               //first rotate
               _curves[i]->rotateGlobal(GMlib::Angle(10),_rot_center,GMlib::Vector<float,3>(1.0,0.0,0.0));
               vec_to_c=_curve_org->getGlobalPos()-_curves[_ctc[i]]->getGlobalPos();
               _curves[_ctc[i]]->translateGlobal(vec_to_c*(1.0f/float(145.0f-_scene_frames)));
           }
           if (_scene_frames==144)
           {
               _scene_frames=0;
               _scene = 8;
           }
           break;
        }
        //unscale + move to origins
        case 8:
        {
            _scene_frames++;
           for (auto i=0; i<_ctc.size(); i++)
           {
               if (_scene_frames==1)
               {
                   _dist_ratio[_ctc[i]]=(_c_origins[_ctc[i]]-_curves[_ctc[i]]->getGlobalPos()).getLength();
                   _scales_ratio[_ctc[i]]=powf((1.0f/_curves[_ctc[i]]->getScale()[0]),1/150.0f);
               }
               _curves[_ctc[i]]->scale(_scales_ratio[_ctc[i]]);
               vec_to_c=_c_origins[_ctc[i]]-_curves[_ctc[i]]->getGlobalPos();
               if (vec_to_c.getLength()>0.0f)
                _curves[_ctc[i]]->translateGlobal(vec_to_c*(1.0f/150.0f)*(_dist_ratio[_ctc[i]]/vec_to_c.getLength()));

           }
           if (_scene_frames==150)
           {
               _scene_frames=0;
               _scene = 11;
           }
           break;
        }
    }//end switch
//qDebug()<<_scene;
    _frame_count++;
    }
    this->sample(this->_visu[0].size(),1);
    this->setEditDone();

}



};



//#include "GERBSCurve.c"

#endif // GERBSCurve_H
