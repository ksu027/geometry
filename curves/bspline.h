#ifndef NEWCURVES_BSPLINE_H
#define NEWCURVES_BSPLINE_H

#include <parametrics/gmpcurve.h>
// gmlib
#include <core/containers/gmdmatrix.h>
#include <scene/selector/gmselector.h>
#include <scene/visualizers/gmselectorgridvisualizer.h>



#include <QtDebug>

namespace NewCurves {

//template <typename T, int n>
//class Selector;

//template <typename T>
//class SelectorGridVisualizer;

template <typename T>
class BSpline : public GMlib::PCurve<T,3> {
    GM_SCENEOBJECT(BSpline)

    public:
        // use control points - and generate a knotvector
        BSpline(const GMlib::DVector<GMlib::Vector<T,3>> & c);
        // use least square to make n control points - and generate a knotvector
        BSpline(const GMlib::DVector<GMlib::Vector<T,3>> & p, int n);

        void edit( int selector, const GMlib::Vector<T,3>& dp) override;
        void toggleSelectors() override;
        void showSelectors( T radius = T(1), bool grid = true,
                            const GMlib::Color& selector_color = GMlib::GMcolor::darkRed(),
                            const GMlib::Color& grid_color = GMlib::GMcolor::darkRed()) override;
        void hideSelectors() override;
        bool isClosed() const override;

        void replot() const override;


    protected:
        void eval(T t, int d, bool l) const override;
        T getStartP() const override;
        T getEndP() const override;

        void updateSamples() const;
    private:
        void genKnotVector();
        void genControlPoints(const GMlib::DVector<GMlib::Vector<T,3>> & p);
        GMlib::DMatrix<T> calcBMatrix();
        int  findI(T t) const;
        T calcW(const int& d, const int& i, T t) const;
        GMlib::Vector<T, 3> calcBasis(T t, int i) const;



        GMlib::DVector<GMlib::Vector<T, 3>> _c; // control points
        std::vector<T> _t;                      // knot points
        int _n;                 // number of control points
        int _m;                 // number of points in the input set of points
        // _m should be >> _n
        const int _d = 2;       // degree
        const int _k = _d + 1;  // order = degree+1 = number of control points

        //this is from gmpbsplinecurve
        bool                         _selectors=false;        //!< Mark if we have selectors or not
        bool                         _grid;             //!< Mark if we have a selector grid or not
        GMlib::SelectorGridVisualizer<T>*   _sgv= 0x0;           //!< Selectorgrid
        std::vector<GMlib::Selector<T,3>*>  _s;                //!< A set of selectors (spheres)
        T                            _selector_radius;
        GMlib::Color                        _selector_color;
        GMlib::Color                        _grid_color;
//        mutable std::vector<GMlib::DMatrix<T>> _pre;
        mutable bool                    _c_moved;    //!< Mark that we are editing, moving controll points
        struct EditSet {
           int         ind;            //!< Index of a selector and thus a control point
           GMlib::Vector<T,3> dp;             //!< The distance the control point has been moved
           EditSet(int i, const GMlib::Vector<T,3>& d): ind(i),dp(d){}
        };
        mutable std::vector<EditSet>    _pos_change; //!< The step vector of control points that has been moved
        mutable bool                    _coord_ch;

};


template <typename T>
BSpline<T>::BSpline(const GMlib::DVector<GMlib::Vector<T, 3>>& c)
    : _c(c)
    , _n(c.getDim())
{
    genKnotVector();
}

template <typename T>
BSpline<T>::BSpline(const GMlib::DVector<GMlib::Vector<T, 3>>& p, int n)
    : _m(p.getDim())
{
    _n = n; //_n should be not less than 3
    genKnotVector();
    genControlPoints(p);
}


template <typename T>
T BSpline<T>::getStartP() const
{
    return _t[_d]; //returns the first value of the domain, which is a d-th value of the knot points
}

template <typename T>
T BSpline<T>::getEndP() const
{
    return _t[_n]; //returns the last value of the domain in the knots
}

template <typename T>
bool BSpline<T>::isClosed() const {
    //in our case is always open due to the implemented knot-vector logic
    return false;
}

template <typename T>
void BSpline<T>::genKnotVector()
{
    // dimension
    _t.resize(_n + _k);

    // first three elements = 0
    T knot = T(0);
    _t[0] = knot;
    _t[1] = knot;
    _t[2] = knot;
    knot += T(1);

    // Knots between first three and last three distributed
    for (std::size_t i = _k; i < _t.size()-_k; i++) {
        _t[i] = knot;
        knot += T(1);
    }

    // last three elements are the same
    _t[_t.size()-_k]   = knot;
    _t[_t.size()-_k+1] = knot;
    _t[_t.size()-_k+2] = knot;

    //for (auto t:_t) qDebug()<<t;
}

template <typename T>
int BSpline<T>::findI(T t) const
{
    // t is supposed to be not less than third element of knots as 3 first are 0,
    // so i is starting from 2 or _d
    auto i = _d;
    // we go to _t.size-3, as last 3 are the same
    while (i<_t.size()-_k && _t[i] <= t)
        i++;

    return i-1;

}

template <typename T>
T BSpline<T>::calcW(const int& d, const int& i, T t) const
{
    // W_{d,i}(t) = (t - t_i)/(t_{i+d} - t_i)
    return (t - _t[i]) / (_t[i + d] - _t[i]);
}

template <typename T>
void BSpline<T>::eval(T t, int d, bool /*l*/) const
{
    // in our case d is always 2 => change it hardcoded
    d=_d;
    this->_p.setDim(_d + 1);

    int i = findI(t);
    auto b = calcBasis(t, i);

    this->_p[0] = b[0] * _c[i - 2] + b[1] * _c[i - 1] + b[2] * _c[i];
    //qDebug()<<"I am in eval";
    //qDebug()<<"e_c[4]= "<<_c[4];
}

template <typename T>
GMlib::Vector<T, 3> BSpline<T>::calcBasis(T t, int i) const
{
    // hardcode calculation of Ws
    auto W_1_i = calcW(1, i, t);
    auto W_2_i = calcW(2, i, t);
    auto W_2_im1 = calcW(2, i - 1, t);

    // hardcode calculation of basis
    GMlib::Vector<T, 3> basis;
    basis[0] = (1 - W_1_i) * (1 - W_2_im1);
    basis[1] = ((1 - W_1_i) * W_2_im1+W_1_i*(1-W_2_i));
    basis[2] = W_1_i * W_2_i;

    return basis;
}

template <typename T>
GMlib::DMatrix<T> BSpline<T>::calcBMatrix()
{
    //creates B matrix of mxn filled with zeros first
    //then in each row:
    //  B[row][0]=0,...,B[row][i-3]=0,B[row][i-2]!=0,B[row][i-1]!=0,B[row][i]!=0,B[row][i+1]=0,...,B[row][n]=0
    //
    auto B = GMlib::DMatrix<T>(_m, _n, T(0));
    GMlib::Vector<T, 3> basis;
    const T t0 = getStartP();
    const T t_row = (getEndP() - t0) / (_m - 1); //dispossision of t in a row
    int i;
    T cur_t;

    for (int j = 0; j < _m; j++) {
        cur_t = t0 + j * t_row;
        i = findI(cur_t);
        basis = calcBasis(cur_t, i);
        for (int k = i - _d; k <= i; k++) {
            B[j][k] = basis[k - i + _d]; //
        }
    }
    return B;
}

template <typename T>
void BSpline<T>::genControlPoints(const GMlib::DVector<GMlib::Vector<T, 3>>& p)
{
    //B*_c=p
    //B^t*B*_c=B^t*p
    auto B = calcBMatrix();
    auto B_t = B;
    B_t.transpose();

    auto A = B_t * B;
    _c = A.invert() * B_t * p;
}
//---------------------------------------------------------------------
//this is from gmpbsplinecurve with some simplification to make selectors work
//---------------------------------------------------------------------
/*! void PBSplineCurve<T>::toggleSelectors()
 *  To toggle the selectors and selector grid.
 */
template <typename T>
void BSpline<T>::toggleSelectors() {

  if(_selectors)  hideSelectors();
  else            showSelectors(_selector_radius, _grid, _selector_color, _grid_color);
}

/*! void PBSplineCurve<T>::showSelectors( T rad, bool grid, const Color& selector_color, const Color& grid_color )
 *  To generate the selecors and optionally the grid.
 *  The frace (optional, d) below means that if you skip the parameter,
 *  the default value is d.
 *
 *  \param[in]  rad - (optional, 1) Radius of the selectors
 *  \param[in]  grid - (optional, true)  Do we plot the grid also?
 *  \param[in]  selector_color (optional, darkBlue) Color of the selectors
 *  \param[in]  grid_color (optional, lightGreen) Color of the grid
 */
template <typename T>
void BSpline<T>::showSelectors( T rad, bool grid, const GMlib::Color& selector_color, const GMlib::Color& grid_color ) {

    //does not matter what color to pass it is not used in selector.
    //only _default = color , but then it is used nowhere
    if( !_selectors ) {
        _s.resize( _c.getDim() );

        for( int i = 0; i < _c.getDim(); i++ )
        {
            if(this->isScaled())
                this->insert( _s[i] = new GMlib::Selector<T,3>( _c[i], i, this, this->_scale.getScale(), rad, selector_color ) );
            else
                this->insert( _s[i] = new GMlib::Selector<T,3>( _c[i], i, this, rad, selector_color ) );
        //thanks to Gustav for the help in coloring the selectors
            _s[i]->removeVisualizer(_s[i]->getVisualizers()[0]);
            _s[i]->insertVisualizer(new GMlib::SelectorVisualizer(
                        rad,GMlib::Material(selector_color*0.25,selector_color*0.7,selector_color, 10.f)
                            ));
        }
         _selectors       = true;
    }
    _selector_radius = rad;
    _selector_color  = selector_color;

    if( grid ) {
        if(!_sgv) _sgv = new GMlib::SelectorGridVisualizer<T>;
        _sgv->setSelectors( _c, 0, false);
        _sgv->setColor( grid_color );
        GMlib::SceneObject::insertVisualizer( _sgv );
        this->setEditDone();
    }
    _grid_color      = grid_color;
    _grid            = grid;
}


/*! void PBSplineCurve<T>::hideSelectors()
 *  To hide and delete selectors and hide and reset the selector grid
 */
template <typename T>
void BSpline<T>::hideSelectors() {

    // Remove Selectors
    if( _selectors ) {
        for( uint i = 0; i < _s.size(); i++ ) {
            this->remove( _s[i] );
            delete _s[i];
        }
        _s.clear();
        _selectors = false;
    }

    // Hide Selector Grid
    if(_sgv) {
      GMlib::SceneObject::removeVisualizer( _sgv );
      _sgv->reset();
    }
}

template <typename T>
void BSpline<T>::edit( int selector_id, const GMlib::Vector<T,3>& dp ) {

  _c_moved = true;
    if( this->_parent ){
        this->_parent->edit( this );
        _coord_ch = true;
    }
    _pos_change.push_back(EditSet(selector_id, dp));
    this->setEditDone(true);
  _c_moved = false;
}


template <typename T>
void  BSpline<T>::updateSamples() const {
//this one should change those selectors and respectfully control points, which were dislocated
    while(_pos_change.size()>0) {
        EditSet es = _pos_change.back();
        for(int i=_pos_change.size()-2; i>=0; i--)
        {
            if (_pos_change[i].ind == es.ind) {
                es.dp += _pos_change[i].dp;
                _pos_change.erase(_pos_change.begin()+i);
            }
        }
        //qDebug()<<"c["<<es.ind<<"]= "<<_c[es.ind];
        //did not work without this preSample thing
        this->preSample(this->_visu[0],this->_visu.no_sample,this->_visu.no_derivatives, this->getParStart(), this->getParEnd() );
        this->setEditDone();
        _pos_change.pop_back();
    }
}

template <typename T>
void BSpline<T>::replot() const {
    updateSamples();
    GMlib::PCurve<T,3>::replot();
}


}


//#include "bspline.c"

#endif // BSPLINE_H
