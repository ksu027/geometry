#ifndef NEWCURVES_BSPLINE_H
#define NEWCURVES_BSPLINE_H

#include <parametrics/gmpcurve.h>

namespace NewCurves {

template <typename T>
class BSpline : public GMlib::PCurve<T,3> {
    GM_SCENEOBJECT(BSpline)

    public:
        // use c as control points - and generate a knotvector
        BSpline(const GMlib::DVector<GMlib::Vector<T,3>> & c);
        // use least square to make n control points - and generate a knotvector
        BSpline(const GMlib::DVector<GMlib::Vector<T,3>> & p, int n);
        //BSpline(const BSpline<T> & copy);

        bool isClosed() const override;
    protected:
        void eval(T t, int d, bool l) const override;
        T getStartP() const override;
        T getEndP() const override;
    private:
        void genKnotVector();
        void makeControlPoints(const GMlib::DVector<GMlib::Vector<T, 3>>& p);
        GMlib::DMatrix<T> makeAMatrix();
        GMlib::Vector<T, 4> makeBasis(T t, int& i) const;
        T calcW(const int& d, const int& i, T t) const;
        int findI(T t) const;


        GMlib::DVector<GMlib::Vector<T, 3>> _c; // control points
        std::vector<T> _t;                      // knot points
        int _n;                 // number of control points
        int _m;                 // number of points in the input set of points
        const int _d = 3;       // degree
        const int _k = _d + 1;  // order = degree+1 = number of control points
};

}

#include "bspline.c"

#endif // BSPLINE_H
