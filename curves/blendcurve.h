#ifndef NEWCURVES_BLENDCURVE_H
#define NEWCURVES_BLENDCURVE_H

#include <parametrics/gmpcurve.h>
#include <QtDebug>
#include <memory>

namespace NewCurves {

template <typename T>
class BlendCurve : public GMlib::PCurve<T,3> {
    GM_SCENEOBJECT(BlendCurve)

    public:
        // gets two PCurves and overlaping ratio, creates a new one
        BlendCurve(GMlib::PCurve<T,3> * curve1, GMlib::PCurve<T,3> * curve2, const double overlap);
        bool isClosed() const override;
    protected:
        void eval(T t, int d, bool l) const override;
        T getStartP() const override;
        T getEndP() const override;
        void          localSimulate(double dt) override;
    private:
        T bFun(T t) const;
        GMlib::PCurve<T,3> * _curve1;
        GMlib::PCurve<T,3> * _curve2;
        double _overlap;

        T _curve1_length;
        T _curve2_length;
        int _frame_count=0;

      };

template <typename T>
BlendCurve<T>::BlendCurve(GMlib::PCurve<T,3> * curve1, GMlib::PCurve<T,3> * curve2, const double overlap)
    : _curve1(curve1)
    , _curve2(curve2)
    , _overlap(overlap)
{
    auto s1 = _curve1/*->getStartP()*/->getParStart();
    auto e1 = _curve1/*->getEndP()*/->getParEnd();
    auto d1 = e1-s1;
    auto s2 = _curve2/*->getStartP()*/->getParStart();
    auto e2 = _curve2/*->getEndP()*/->getParEnd();
    auto d2 = e2-s2;
    auto scaled_overlap = _overlap * double(d1)/double(d2); //scale overlap for the second curve so it is equal to first one
    //segments' borders
    auto s_1_start = s1;                        // start of first curve
    auto s_1_end   = e1-d1*_overlap;            // end of first curve - overlap
    auto s_2_end   = e1;                        // end of first curve
    auto s_3_end   = e1+d2*(1-scaled_overlap);


    _curve1_length = _curve1->getCurveLength();
    _curve2_length = _curve2->getCurveLength();
//    qDebug()<<"s1: "<<s1;
//    qDebug()<<"e1: "<<e1;
//    qDebug()<<"s2: "<<s2;
//    qDebug()<<"e2: "<<e2;
//    qDebug()<<"se1: "<<s_1_start<<" <=t<"<<s_1_end;
//    qDebug()<<"se2: "<<s_1_end<<" <=t<"<<s_2_end;
//    qDebug()<<"se3: "<<s_2_end<<" <=t<"<<s_3_end;
}

template <typename T>
bool BlendCurve<T>::isClosed() const {
    //hardcoded always open
    return false;
}

template <typename T>
T BlendCurve<T>::getStartP() const
{
    return _curve1->getParStart();
}

template <typename T>
T BlendCurve<T>::getEndP() const
{
    return _curve1->getParEnd()+(_curve2->getParEnd()-_curve2->getParStart())*(1-_overlap*(_curve1->getParEnd()-_curve1->getParStart())/(_curve2->getParEnd()-_curve2->getParStart()));
}

template <typename T>
T BlendCurve<T>::bFun(T t) const
{
    //Symmetric RB-function
    return pow(t,2)/(2*pow(t,2)-2*t+1);
    //return (t*t)/(2*(t*t)-2*t+1);
}

template <typename T>
void BlendCurve<T>::eval(T t, int d, bool /*l*/) const
{
    // in our case d is always 2 => change it hardcoded
    d=2;
    this->_p.setDim(d+1);
    //start-end of curves
    auto s1 = _curve1/*->getStartP()*/->getParStart();
    auto e1 = _curve1/*->getEndP()*/->getParEnd();
    auto d1 = e1-s1;
    auto s2 = _curve2/*->getStartP()*/->getParStart();
    auto e2 = _curve2/*->getEndP()*/->getParEnd();
    auto d2 = e2-s2;
    auto scaled_overlap = _overlap * double(d1)/double(d2); //scale overlap for the second curve so it is equal to first one
    //qDebug()<<"scaled = "<<scaled_overlap<<"; overlap = "<<_overlap<<";";
    //segments' borders
    auto s_1_start = s1;                        // start of first curve
    auto s_1_end   = e1-d1*_overlap;            // end of first curve - overlap
    auto s_2_end   = e1;                        // end of first curve
    auto s_3_end   = e1+d2*(1-scaled_overlap);  // end of first + part of second after overlap
    //separate computations for each segment
    if (s_1_start<=t && t<s_1_end)
    {
        //return just t value of curve1
        //change to evaluate parent
        this->_p[0] = _curve1->evaluateParent(t,0)[0];
        //this->_p[0] = (*_curve1).getPosition(t);
        //qDebug()<<"seg_1_t_"<<t;
    }
    else if (s_1_end<=t && t<s_2_end)
    {
        //qDebug()<<"seg_2_t_"<<t;
        auto y = 1-(e1-t)/(d1*_overlap);
        //auto x  = s2+y*d2*_overlap;
        T x  = t - e1 + d2*scaled_overlap;
        //T x  = t - e1 + d2*_overlap;
        this->_p[0] = (1-bFun(y))*_curve1->evaluateParent(t,0)[0]+bFun(y)*_curve2->evaluateParent(x,0)[0];
        //this->_p[0] = (1-bFun(y))*(*_curve1).getPosition(t)+bFun(y)*(*_curve2).getPosition(x);
    }
    else if (s_2_end<=t && t<s_3_end)
    {

        //return "scaled" t value of curve2
        T x  = t - e1 + d2*scaled_overlap;
        //T x  = t - e1 + d2*_overlap;
        this->_p[0] = _curve2->evaluateParent(x,0)[0];
        //this->_p[0] = (*_curve2).getPosition(x);
        //qDebug()<<"seg_3_t_"<<t;
    }
}

template <typename T>
void BlendCurve<T>::localSimulate( double dt ) {
    // this was too expensive
    // the idea was to update the curve after the children curves were updated

//    T new_curve1_length = _curve1->getCurveLength();
//    T new_curve2_length = _curve2->getCurveLength();
//    if (_curve1_length!=new_curve1_length || _curve2_length!=new_curve2_length)
//    {
//        this->sample(400,1);
//        this->setEditDone();
//        _curve1_length=new_curve1_length;
//        _curve2_length=new_curve2_length;
//    }

    //simplified version - update every some frames
//    _frame_count++;
//    if (_frame_count>50){

        this->sample(this->_visu[0].size(),1);
        this->setEditDone();
        _frame_count = 0;
//    }
}

};



//#include "BlendCurve.c"

#endif // BlendCurve_H
