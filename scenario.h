#ifndef SCENARIO_H
#define SCENARIO_H


#include "application/gmlibwrapper.h"

// qt
#include <QObject>

//namespace NewCurves {
//template <typename T> class BSpline;
////class defaultsplinecurve;
//}


class Scenario : public GMlibWrapper {
  Q_OBJECT
public:
  using GMlibWrapper::GMlibWrapper;

  void    initializeScenario() override;
  void    cleanupScenario() override;

public slots:
  void    callDefferedGL();
  void    loadGObject(int);

private:
  void initBSplineControl();

  std::vector<GMlib::SceneObject*> _s_objs;

  //std::shared_ptr<defaultsplinecurve> _controlDefCurve;
  //std::shared_ptr<NewCurves::BSpline<float>> _controlMyCurve;
};


#endif // SCENARIO_H
